package numinwords

import (
	"math"
	"math/big"
	"strings"
)

var units = [20]string{"", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"}
var tens = [10]string{"", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"}
var pows = [4]string{"hundred", "thousand", "lakh", "crore"}

// ParseFloat converts numbers into words using Indian format.
func ParseFloat(n float64) string {

	if n == 0 {
		return "zero"
	}

	if n < 0 {
		return "minus " + ParseFloat(n*-1.0)
	}

	bfNumberA := new(big.Float).SetPrec(128).SetFloat64(n)
	bfNumberB := new(big.Float).SetPrec(128).SetFloat64(100.0)
	bfNumberA.Mul(bfNumberA, bfNumberB)

	i64, _ := bfNumberA.Int64()
	i64 = i64 / 100
	wholePart := i64
	bfNumberA.SetFloat64(n)
	bfNumberB.SetInt64(wholePart)
	bfNumberA.Sub(bfNumberA, bfNumberB)
	decimalPart, _ := bfNumberA.Float64()
	decimalPart100 := math.Round(decimalPart * 100)

	decimalPartInt := int64(decimalPart100)
	decimalPartUnits := decimalPartInt % 10
	decimalPartTens := decimalPartInt / 10

	if decimalPart == 0 {
		return ParseInt(wholePart)
	}
	dTensInWords := ""
	if decimalPartTens == 0 {
		dTensInWords = "zero"
	} else {
		dTensInWords = ParseInt(decimalPartTens)
	}
	dUnitsInWords := ""
	if decimalPartUnits == 0 {
		dUnitsInWords = "zero"
	} else {
		dUnitsInWords = ParseInt(decimalPartUnits)
	}
	return ParseInt(wholePart) + " point " + dTensInWords + " " + dUnitsInWords
}

// ParseInt converts numbers into words using Indian format.
func ParseInt(n int64) string {
	var inWords string
	var i int
	var s string
	if n < 0 {
		return "negative number"
	}
	if n < 100 {
		inWords = below100(n)
	} else if (n >= 100) && (n <= 999) {
		inWords = below999(n)
	} else if (n >= 1000) && (n <= 99999) {
		inWords = below99999(n)
	} else if (n >= 100000) && (n <= 9999999) {
		inWords = belowcrore(n)
	} else {
		s = abovecrore(n)
		i = strings.Count(s, " and")
		if n%100 == 0 {
			i = i + 1
		}
		inWords = strings.Replace(s, " and", "", i-1)
	}
	return inWords
}

func below100(n int64) string {
	var inWords string
	if n == 0 {
		return ""
	} else if n < 20 {
		return units[n]
	} else {
		inWords = tens[n/10] + " " + units[n%10]
	}
	return strings.TrimSpace(inWords)
}

func below999(n int64) string {
	var temp1 int64
	var temp2 string
	var inWords string
	temp1 = n % 100
	if units[n/100] != "" {
		temp2 = units[n/100] + " " + pows[0]
	} else {
		temp2 = units[n/100]
	}
	if below100(temp1) == "" {
		inWords = temp2
	} else {
		if temp2 == "" {
			inWords = "and" + " " + below100(temp1)
		} else {
			inWords = temp2 + " " + "and" + " " + below100(temp1)
		}
	}
	return inWords
}

func below99999(n int64) string {
	var temp1 int64
	var temp2 int64
	var inWords string
	temp1 = n % 1000
	temp2 = n / 1000
	if below100(temp2) != "" {
		if below999(temp1) == "" {
			inWords = below100(temp2) + " " + pows[1]
		} else {

			inWords = below100(temp2) + " " + pows[1] + " " + below999(temp1)
		}
	} else {
		inWords = below999(temp1)
	}
	return inWords
}

func belowcrore(n int64) string {
	var temp1 int64
	var temp2 int64
	var inWords string
	temp1 = n % 100000
	temp2 = n / 100000
	if below100(temp2) != "" {
		if below99999(temp1) == "" {
			inWords = below100(temp2) + " " + pows[2]
		} else {
			inWords = below100(temp2) + " " + pows[2] + " " + below99999(temp1)
		}
	} else {
		inWords = below99999(temp1)
	}
	return inWords
}

func abovecrore(n int64) string {
	var temp1 int64
	var temp2 int64
	var inWords string
	temp1 = n % 10000000
	temp2 = n / 10000000
	if ParseInt(temp2) != "" {
		if belowcrore(temp1) == "" {
			inWords = ParseInt(temp2) + " " + pows[3]
		} else {
			inWords = ParseInt(temp2) + " " + pows[3] + " " + belowcrore(temp1)
		}
	}
	return inWords
}
